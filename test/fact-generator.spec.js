const { expect } = require('chai')
const { Builder, By, Key, WebElement, ThenableWebDriver, Browser } = require('selenium-webdriver')

const BASE_URL = 'https://www.randomfactgenerator.com/'

const sleep = ms => new Promise(r => setTimeout(r, ms));

describe('UI Tests', () => {
  let driver
  before(async () => {
    driver = await new Builder().forBrowser(Browser.FIREFOX).build()
    await driver.get(BASE_URL)
  })

  it('Should open the webpage with the correct title', async () => {
    const title = await driver.getTitle()
    expect(title).to.equal('Random Fact Generator')
  })

  it('Should render correctly', async () => {
    const h1 =  await driver.findElement(By.css('h1')).getText()
    expect(h1).to.equal('Random Fact Generator')
  })

  it('Should generate new fact when click the button', async () => {
    await sleep(2500) // Wait because the page reload the first fact

    const newFactButton = await driver.findElement(By.id('comp-l3lze6ve')) // Get button to generate new fact
    const initialFact = await driver.findElement(By.css('h2')).getText() // Get the initial fact
    
    newFactButton.click() // Generate new fact

    await sleep(2500) // Wait to the new fact

    const newFact = await driver.findElement(By.css('h2')).getText() // Get the new fact
    expect(initialFact).to.not.equal(newFact)
  })

  after(() =>{
    driver.quit() // close browser
  })

})